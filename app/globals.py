global animals
animals = [line.rstrip('\n') for line in open('app/resources/animals.txt', 'r')]
global adjectives
adjectives = [line.rstrip('\n') for line in open('app/resources/adjectives.txt', 'r')]
