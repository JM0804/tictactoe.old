from app import db

class Room(db.Model):
	roomID = db.Column(db.Integer, primary_key=True, index=False, autoincrement=True)
	roomKey = db.Column(db.String, index=False)
	maxPlayers = db.Column(db.Integer, index=False, default=2)
	numPlayers = db.Column(db.Integer, index=False)
	board = db.Column(db.String, index=False)
	playerTurn = db.Column(db.Integer, index=False)
	spectatorKey = db.Column(db.String, index=False)
	lastMove = db.Column(db.Integer, index=False)
	requestingUndoMove = db.Column(db.Integer, index=False)
	settings_allowSpectators = db.Column(db.Boolean, index=False, default=False)

	def __repr__(self):
		return '<Room roomID={}, roomKey={}, players={}/{}, board={}, playerTurn={}>'.format(\
			self.roomID, self.roomKey, self.numPlayers, self.maxPlayers, self.board, self.playerTurn)

class Player(db.Model):
	playerID = db.Column(db.Integer, primary_key=True, index=False, autoincrement=True)
	playerNum = db.Column(db.Integer, index=False)
	roomID = db.Column(db.ForeignKey(Room.roomID), index=False)
	playerKey = db.Column(db.String, index=False, default='')

	def __repr__(self):
		return '<Player playerID={}, playerKey={} playerNum={}, roomID={}'.format(\
			self.playerID, self.playerKey, self.playerNum, self.roomID)
