import json
import os
import random
import csv
from collections import defaultdict
from geventwebsocket import WebSocketError

from app import app, db, globals
from app.models import Room, Player

# Hold every WebSocket per room
wsList = defaultdict(list)
privilegeList = defaultdict(list)

def send(sockets, data, roomID=None):
	for socket in sockets:
		try:
			print('sending ' + data)
			socket.send(data)
		except WebSocketError:
			print('socket is closed, so removing')
			if roomID:
				wsList[roomID].remove(socket)
	return

def generateTwoWordKey():
	"""Generate a two-word key (e.g. ecstatic panda)"""
	animals = globals.animals
	adjectives = globals.adjectives
	return random.choice(adjectives) + ' ' + random.choice(animals)

def updateRoomClients(roomID):
	"""Push board and playerTurn updates out to every connected WebSocket in the room"""
	print('updating room clients for room #' + str(roomID))
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	if room:
		result = {
			'event':'update',
			'board':room.board,
			'playerTurn':room.playerTurn
		}
		result = json.dumps(result)
		send(wsList[roomID][:], result, roomID)
	return

def updateGameOver(roomID, winner):
	"""Either someone has won or the game resulted in a draw.
	Push that result to every connected instance in the room
	and delete the associated players and room from the database"""
	print('game over in room #' + str(roomID))
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	players = db.session.query(Player).filter_by(roomID=roomID).all()
	result = {
		'event':'gameover',
		'winner':winner
	}
	result = json.dumps(result)
	send(wsList[roomID][:], result, roomID)
	# Delete the players and room from the database
	for player in players:
		db.session.delete(player)
	db.session.delete(room)
	db.session.commit()
	return

def kickSpectator(roomID, ws):
	"""Send a kicked event to the WebSocket and remove it from the WebSocket list"""
	result = {
		'event':'kicked'
	}
	result = json.dumps(result)
	send([ws], result, roomID)
	wsList[roomID].remove(ws)
	return

def kickSpectators(roomID):
	"""Kick any spectators in the room"""
	print('kicking spectators from room #' + str(roomID))
	for ws in wsList[roomID]:
		if privilegeList[ws] == 0:
			kickSpectator(roomID, ws)
	# Delete all spectators from the database
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	spectators = db.session.query(Player).filter_by(roomID=roomID, playerNum=0).all()
	for spectator in spectators:
		db.session.delete(spectator)
	db.session.commit()
	return

def addClientWebSocket(roomID, ws):
	"""Store the new WebSocket according to its originating room"""
	print('adding new websocket to room #' + str(roomID))
	tempList = wsList[roomID]
	tempList.append(ws)
	wsList[roomID] = tempList
	return

def checkIfWon(roomID):
	"""Check if a player has won, or if the game has resulted in a draw"""
	winner = -1
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	reader = csv.reader([room.board])
	boardList = list(reader)[0]
	print(boardList)
	# Test for either player won
	for playerNum in range(1, 3):
		num = str(playerNum)
		if 	(boardList[0] == num and boardList[1] == num and boardList[2] == num) or \
			(boardList[3] == num and boardList[4] == num and boardList[5] == num) or \
			(boardList[6] == num and boardList[7] == num and boardList[8] == num) or \
			(boardList[0] == num and boardList[3] == num and boardList[6] == num) or \
			(boardList[1] == num and boardList[4] == num and boardList[7] == num) or \
			(boardList[2] == num and boardList[5] == num and boardList[8] == num) or \
			(boardList[0] == num and boardList[4] == num and boardList[8] == num) or \
			(boardList[2] == num and boardList[4] == num and boardList[6] == num):
			updateGameOver(roomID, num)
			return
	# Test for a draw
	full = True
	for cell in boardList:
		if cell == '0':
			full = False
	if full:
		winner = 0
	# Either there is a winner or there has been a draw, so push that to all connected clients
	if winner != -1:
		updateGameOver(roomID, winner)
	return

def updateRoomSettings(roomID, settings):
	"""Alter the room settings and push them to all connected clients"""
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	for setting, value in settings.items():
		if setting == 'settings_allowSpectators':
			boolValue = 0
			if value == 'true':
				boolValue = True
				room.spectatorKey = generateTwoWordKey()
			elif value == 'false':
				boolValue = False
				room.spectatorKey = None
				kickSpectators(room.roomID)
			room.settings_allowSpectators = boolValue
			db.session.commit()
			updateNewSpectatorKey(room.roomID)
	db.session.commit()
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	settings = {
		'settings_allowSpectators':room.settings_allowSpectators
	}
	result = {
		'event':'newroomsettings',
		'settings':settings
	}
	result = json.dumps(result)
	send(wsList[roomID][:], result, roomID)
	return

def updateNewSpectatorKey(roomID):
	"""Notify all players and spectators of a change to the spectatorKey"""
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	result = {
		'event':'newspectatorkey',
		'key':room.spectatorKey
	}
	result = json.dumps(result)
	send(wsList[roomID][:], result, roomID)
	return

def updatePlayerCount(roomID):
	"""Notify all players and spectators of a change of player count"""
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	result = {
		'event':'updateplayercount',
		'count':room.numPlayers
	}
	result = json.dumps(result)
	send(wsList[roomID][:], result, roomID)
	return

def updateLeftRoom(playerID):
	"""Notify all players that a player has left the room, then close the room and delete the players"""
	player = db.session.query(Player).filter_by(playerID=playerID).first()
	room = db.session.query(Room).filter_by(roomID=player.roomID).first()
	players = db.session.query(Player).filter_by(roomID=player.roomID).all()
	result = {
		'event':'leaveroom',
		'success':True,
		'playerNum':player.playerNum
	}
	result = json.dumps(result)
	send(wsList[player.roomID][:], result, player.roomID)
	# Delete the players and room from the database
	for player in players:
		db.session.delete(player)
	db.session.delete(room)
	db.session.commit()
	return

def updateUndoneMove(roomID, approved):
	"""Inform all users in the room whether the undoing of a move has been approved or not"""
	players = db.session.query(Player).filter_by(roomID=roomID).all()
	room = db.session.query(Room).filter_by(roomID=roomID).first()
	result = {
		'event':'undomove',
		'success':approved,
		'playerNum':room.requestingUndoMove
	}
	result = json.dumps(result)
	send(wsList[roomID][:], result, roomID)
	return

def requestUndoMove(playerID):
	"""Make a request to the other players to undo the move they just made"""
	player = db.session.query(Player).filter_by(playerID=playerID).first()
	players = db.session.query(Player).filter_by(roomID=player.roomID).all()
	room = db.session.query(Room).filter_by(roomID=player.roomID).first()
	room.requestingUndoMove = player.playerNum
	db.session.commit()
	result = {
		'event':'approveundomove',
		'success':True,
		'playerNum':player.playerNum
	}
	result = json.dumps(result)
	send(wsList[player.roomID][:], result, player.roomID)
	return

def handle_websocket(ws):
	print("Handle WebSocket")
	while True:
		message = ws.receive()
		if message is None:
			break
		else:
			print('received: ' + str(message))
			data = json.loads(message)
			# Handle test event
			if data['event'] == 'test':
				wsevent_test(ws, data)
			# Handle getboard event
			elif data['event'] == 'getboard':
				wsevent_getboard(ws, data)
			# Handle createroom event
			elif data['event'] == 'createroom':
				wsevent_createroom(ws, data)
			# Handle enterroom event
			elif data['event'] == 'enterroom':
				wsevent_enterroom(ws, data)
			# Handle rejoinroom event
			elif data['event'] == 'rejoinroom':
				wsevent_rejoinroom(ws, data)
			# Handle move event
			elif data['event'] == 'move':
				wsevent_move(ws, data)
			# Handle spectateroom event
			elif data['event'] == 'spectateroom':
				wsevent_spectateroom(ws, data)
			# Handle updatesettings event
			elif data['event'] == 'updatesettings':
				wsevent_updatesettings(ws, data)
			# Handle leaveroom event
			elif data['event'] == 'leaveroom':
				wsevent_leaveroom(ws, data)
			# Handle requestundomove event
			elif data['event'] == 'requestundomove':
				wsevent_requestundomove(ws, data)
			# Handle respondtoundomoverequest event
			elif data['event'] == 'respondtoundomoverequest':
				wsevent_respondtoundomoverequest(ws, data)

def wsevent_createroom(ws, data):
	"""Handle WebSocket createroom events"""
	print('handling createroom event')
	# Set privilege level for WebSocket
	privilegeList[ws] = 1
	# Create and commit a new Room
	room = Room(roomKey=generateTwoWordKey(), numPlayers=1, board='0,0,0,0,0,0,0,0,0', playerTurn=1)
	db.session.add(room)
	db.session.commit()
	# Create and commit a new Player, tied to the new Room
	player = Player(playerNum=1, roomID=room.roomID, playerKey=generateTwoWordKey())
	db.session.add(player)
	db.session.commit()
	settings = {
		'settings_allowSpectators':room.settings_allowSpectators
	}
	result = {
		'event':'createroom',
		'success':True,
		'roomID':room.roomID,
		'roomKey':room.roomKey,
		'playerID':player.playerID,
		'playerKey':player.playerKey,
		'playerNum':player.playerNum,
		'playerTurn':room.playerTurn,
		'numPlayers':room.numPlayers,
		'maxPlayers':room.maxPlayers,
		'settings':settings
	}
	result = json.dumps(result)
	print('sending ' + result)
	addClientWebSocket(room.roomID, ws)
	send([ws], result, room.roomID)
	return

def wsevent_enterroom(ws, data):
	"""Handle WebSocket enterroom events"""
	print('handling enterroom event')
	# Set privilege level for WebSocket
	privilegeList[ws] = 2
	requestedID = data['roomID']
	requestedKey = data['roomKey']
	result = {}
	room = db.session.query(Room).filter_by(roomID=requestedID).first()
	if room:
		if room.roomKey == requestedKey:
			# key matches, create a Player and add them to the Room if it isn't full
			if room.numPlayers == room.maxPlayers:
				result = {
					'event':'enterroom',
					'success':False,
					'message':'Room is full!'
				}
			else:
				room.numPlayers = room.numPlayers + 1
				db.session.commit()
				player = Player(playerNum=room.numPlayers, roomID=room.roomID, playerKey=generateTwoWordKey())
				db.session.add(player)
				db.session.commit()
				settings = {
					'settings_allowSpectators':room.settings_allowSpectators
				}
				result = {
					'event':'enterroom',
					'success':True,
					'playerID':player.playerID,
					'playerNum':player.playerNum,
					'playerKey':player.playerKey,
					'spectatorKey':room.spectatorKey,
					'board':room.board,
					'playerTurn':room.playerTurn,
					'numPlayers':room.numPlayers,
					'maxPlayers':room.maxPlayers,
					'settings':settings
				}
				addClientWebSocket(room.roomID, ws)
				updatePlayerCount(room.roomID)
		else:
			# key doesn't match, give the user an error
			result = {
				'event':'enterroom',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
	else:
		# room doesn't exist, give the user an error
		result = {
			'event':'enterroom',
			'success':False,
			'message':'Room doesn\'t exist! Please check the ID and try again'
		}
	result = json.dumps(result)
	send([ws], result)
	return

def wsevent_rejoinroom(ws, data):
	"""Handle WebSocket rejoinroom events"""
	print('handling rejoinroom event')
	requestID = data['playerID']
	requestKey = data['playerKey']
	result = {}
	player = db.session.query(Player).filter_by(playerID=requestID).first()
	if player:
		if requestKey == player.playerKey:
			room = db.session.query(Room).filter_by(roomID=player.roomID).first()
			if room:
				settings = {
					'settings_allowSpectators':room.settings_allowSpectators
				}
				result = {
					'event':'rejoinroom',
					'success':True,
					'roomID':room.roomID,
					'roomKey':room.roomKey,
					'playerNum':player.playerNum,
					'spectatorKey':room.spectatorKey,
					'board':room.board,
					'playerTurn':room.playerTurn,
					'numPlayers':room.numPlayers,
					'maxPlayers':room.maxPlayers,
					'settings':settings
				}
				addClientWebSocket(room.roomID, ws)
			else:
				result = {
					'event':'rejoinroom',
					'success':False,
					'message':'The room is no longer open!'
				}
		else:
			result = {
				'event':'rejoinroom',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
	else:
		result = {
			'event':'rejoinroom',
			'success':False,
			'message':'Incorrect ID! Please check the ID and try again'
		}
	result = json.dumps(result)
	send([ws], result)
	return

def wsevent_updatesettings(ws, data):
	"""Handle WebSocket updatesettings events"""
	print('handling updatesettings event')
	playerID = data['playerID']
	playerKey = data['playerKey']
	settings = data['settings']
	result = {}
	player = db.session.query(Player).filter_by(playerID=playerID).first()
	if player:
		if playerKey == player.playerKey:
			room = db.session.query(Room).filter_by(roomID=player.roomID).first()
			if room:
				if player.playerNum == 1:
					result = {
						'event':'updatesettings',
						'success':True
					}
					updateRoomSettings(room.roomID, settings)
				else:
					result = {
						'event':'updatesettings',
						'success':False,
						'message':'You don\'t have admin rights!'
					}
			else:
				result = {
					'event':'updatesettings',
					'success':False,
					'message':'The room is no longer open!'
				}
		else:
			result = {
				'event':'updatesettings',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
	else:
		result = {
			'event':'updatesettings',
			'success':False,
			'message':'Incorrect ID! Please check the ID and try again'
		}
	success = result['success']
	result = json.dumps(result)
	send([ws], result)
	return

def wsevent_spectateroom(ws, data):
	"""Handle WebSocket spectateroom events"""
	print('handling spectateroom event')
	# Set privilege level for WebSocket
	privilegeList[ws] = 0
	spectateID = data['roomID']
	spectateKey = data['spectateKey']
	result = {}
	room = db.session.query(Room).filter_by(roomID=spectateID).first()
	if room:
		if room.spectatorKey == spectateKey:
			# key matches, create a Player as a spectator and add them to the Room
				player = Player(playerNum=0, roomID=room.roomID)
				db.session.add(player)
				db.session.commit()
				result = {
					'event':'spectateroom',
					'success':True,
					'board':room.board,
					'playerTurn':room.playerTurn
				}
				addClientWebSocket(room.roomID, ws)
		else:
			# key doesn't match, give the user an error
			result = {
				'event':'spectateroom',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
	else:
		# room doesn't exist, give the user an error
		result = {
			'event':'spectateroom',
			'success':False,
			'message':'Room doesn\'t exist! Please check the ID and try again'
		}
	result = json.dumps(result)
	send([ws], result)
	return
	

def wsevent_move(ws, data):
	"""Handle WebSocket move events"""
	print('handling move event')
	requestID = data['playerID']
	requestKey = data['playerKey']
	result = {}
	player = db.session.query(Player).filter_by(playerID=requestID).first()
	if player:
		if requestKey == player.playerKey:
			room = db.session.query(Room).filter_by(roomID=player.roomID).first()
			if room:
				if player.playerNum == room.playerTurn:
					if room.numPlayers == room.maxPlayers:
						# Authenticated, and it is their turn,
						# and the room is full,
						# so get the requested cell, and the
						# current status of the board as a list
						requestedCell = int(data['cell'])
						reader = csv.reader([room.board])
						boardList = list(reader)[0]
						if boardList[requestedCell] == '0':
							# Previously unselected cell, place mark here
							boardList[requestedCell] = player.playerNum
							# Convert it back to CSV and write it to the room object
							room.board = ','.join(map(str, boardList))
							# Store the cell of the last move
							room.lastMove = requestedCell
							# Next player's turn
							room.playerTurn = room.playerTurn+1;
							if room.playerTurn > room.maxPlayers:
								room.playerTurn = 1
							db.session.commit()
							result = {
								'event':'move',
								'cellID':requestedCell,
								'success':True
							}
						else:
							result = {
								'event':'move',
								'success':False,
								'message':'This cell is already taken!'
							}
					else:
						result = {
							'event':'move',
							'success':False,
							'message':'Waiting for the other players to join!'
						}
				else:
					result = {
						'event':'move',
						'success':False,
						'message':'It isn\'t your turn!'
					}
			else:
				result = {
					'event':'move',
					'success':False,
					'message':'The room is no longer open!'
				}
		else:
			result = {
				'event':'move',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
	else:
		result = {
			'event':'move',
			'success':False,
			'message':'Incorrect ID! Please check the ID and try again'
		}
	success = result['success']
	result = json.dumps(result)
	send([ws], result)
	if success:
		checkIfWon(room.roomID)
		updateRoomClients(room.roomID)
	return

def wsevent_getboard(ws, data):
	"""Handle WebSocket getboard events"""
	print('handling getboard event')
	requestID = data['playerID']
	requestKey = data['playerKey']
	result = {}
	player = db.session.query(Player).filter_by(playerID=requestID).first()
	if player:
		if requestKey == player.playerKey:
			room = db.session.query(Room).filter_by(roomID=player.roomID).first()
			if room:
				result = {
					'event':'getboard',
					'success':True,
					'board':room.board,
					'playerTurn':room.playerTurn
				}
			else:
				result = {
					'event':'getboard',
					'success':False,
					'message':'The room is no longer open!'
				}
		else:
			result = {
				'event':'getboard',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
	else:
		result = {
			'event':'getboard',
			'success':False,
			'message':'Incorrect ID! Please check the ID and try again'
		}
	result = json.dumps(result)
	send([ws], result)
	return

def wsevent_leaveroom(ws, data):
	"""Handle WebSocket leaveroom events"""
	print('handling leaveroom event')
	result = False
	requestID = data['playerID']
	requestKey = data['playerKey']
	player = db.session.query(Player).filter_by(playerID=requestID).first()
	if player:
		if requestKey == player.playerKey:
			room = db.session.query(Room).filter_by(roomID=player.roomID).first()
			if room:
				updateLeftRoom(requestID)
			else:
				result = {
					'event':'leaveroom',
					'success':False,
					'message':'The room is no longer open!'
				}
				result = json.dumps(result)
		else:
			result = {
				'event':'leaveroom',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
			result = json.dumps(result)
	else:
		result = {
			'event':'leaveroom',
			'success':False,
			'message':'Incorrect ID! Please check the ID and try again'
		}
		result = json.dumps(result)
	if result:
		send([ws], result, room.roomID)
	return

def wsevent_requestundomove(ws, data):
	"""Handle WebSocket requestundomove events"""
	print('handling requestundomove event')
	result = {}
	requestID = data['playerID']
	requestKey = data['playerKey']
	player = db.session.query(Player).filter_by(playerID=requestID).first()
	if player:
		if requestKey == player.playerKey:
			room = db.session.query(Room).filter_by(roomID=player.roomID).first()
			if room:
				if room.playerTurn != player.playerNum:
					reader = csv.reader([room.board])
					boardList = list(reader)[0]
					# If the board isn't empty:
					if not all(cell == '0' for cell in boardList):
						if int(boardList[room.lastMove]) == player.playerNum:
							requestUndoMove(requestID)
						else:
							result = {
								'event':'requestundomove',
								'success':False,
								'message':'You can\'t undo your move once the other player has made theirs'
							}
							result = json.dumps(result)
					else:
						result = {
							'event':'requestundomove',
							'success':False,
							'message':'There is nothing to undo!'
						}
						result = json.dumps(result)
				else:
					result = {
						'event':'requestundomove',
						'success':False,
						'message':'You can\'t undo your move until you\'ve made it'
					}
					result = json.dumps(result)
			else:
				result = {
					'event':'requestundomove',
					'success':False,
					'message':'The room is no longer open!'
				}
				result = json.dumps(result)
		else:
			result = {
				'event':'requestundomove',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
			result = json.dumps(result)
	else:
		result = {
			'event':'requestundomove',
			'success':False,
			'message':'Incorrect ID! Please check the ID and try again'
		}
		result = json.dumps(result)
	if result:
		send([ws], result, room.roomID)
	return

def wsevent_respondtoundomoverequest(ws, data):
	""" Handle WebSocket respondtoundomoverequest events"""
	print('handling respondtoundomoverequest event')
	result = {}
	requestID = data['playerID']
	requestKey = data['playerKey']
	player = db.session.query(Player).filter_by(playerID=requestID).first()
	if player:
		if requestKey == player.playerKey:
			room = db.session.query(Room).filter_by(roomID=player.roomID).first()
			if room:
				allowed = data['allow']
				if allowed:
					# remove the last move
					reader = csv.reader([room.board])
					boardList = list(reader)[0]
					boardList[room.lastMove] = 0
					print(boardList)
					room.board = ','.join(map(str, boardList))
					# Go back a turn
					room.playerTurn -= 1
					if room.playerTurn < 1:
						room.maxPlayers
					db.session.commit()
				updateUndoneMove(room.roomID, allowed)
				updateRoomClients(room.roomID)
			else:
				result = {
					'event':'respondtoundomoverequest',
					'success':False,
					'message':'The room is no longer open!'
				}
				result = json.dumps(result)
		else:
			result = {
				'event':'respondtoundomoverequest',
				'success':False,
				'message':'Incorrect key! Please check the key and try again'
			}
			result = json.dumps(result)
	else:
		result = {
			'event':'respondtoundomoverequest',
			'success':False,
			'message':'Incorrect ID! Please check the ID and try again'
		}
		result = json.dumps(result)
	if result:
		send([ws], result, room.roomID)
	return

def wsevent_test(ws, data):
	"""Handle WebSocket test events"""
	print('handling test event')
	result = {
		'event':'test',
		'success':True
	}
	result = json.dumps(result)
	send([ws], result)
	return
