from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class CreateRoomForm(FlaskForm):
	submit = SubmitField('Create Room')

class EnterRoomForm(FlaskForm):
    roomID = StringField('Room ID', validators=[DataRequired()])
    roomKey = StringField('Room Key', validators=[DataRequired()])
    submit = SubmitField('Enter Room')

class RejoinRoomForm(FlaskForm):
	playerID = StringField('Player ID', validators=[DataRequired()])
	playerKey = StringField('Player Key', validators=[DataRequired()])
	submit = SubmitField('Re-join Room')
