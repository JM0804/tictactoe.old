from app import app, db, globals
from app.forms import EnterRoomForm, CreateRoomForm, RejoinRoomForm
from app.functions import *
import datetime
import random

from gevent.pywsgi import WSGIServer
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler
from flask import Flask, render_template, request

@app.route('/')
def index():
	"""Serve the web page"""
	print("Serve webpage")
	return render_template('index.html', joining=False, enterRoomForm=EnterRoomForm(), createRoomForm=CreateRoomForm(), rejoinRoomForm=RejoinRoomForm(), now=datetime.datetime.utcnow)

@app.route('/about')
def about():
	"""Serve the about page"""
	print("Serve about page")
	return render_template('about.html')

@app.route('/join')
@app.route('/join/<joinID>')
@app.route('/join/<joinID>/<joinKey>')
def join(joinID=None, joinKey=None):
	"""Connect the player to a room as a new player, then serve the web page"""
	print("Serve join")
	splitKey = joinKey.split('-')
	joinKey = splitKey[0] + ' ' + splitKey[1]
	if joinID and joinKey:
		print("Join with ID " + str(joinID) + " and key " + joinKey)
	return render_template('index.html', joining=True, joinRoomID=joinID, joinRoomKey=joinKey, now=datetime.datetime.utcnow)

@app.route('/rejoin')
@app.route('/rejoin/<rejoinID>')
@app.route('/rejoin/<rejoinID>/<rejoinKey>')
def rejoin(rejoinID=None, rejoinKey=None):
	"""Connect the player to a room as an existing player, then serve the web page"""
	print("Serve rejoin")
	splitKey = rejoinKey.split('-')
	rejoinKey = splitKey[0] + ' ' + splitKey[1]
	if rejoinID and rejoinKey:
		print("Join with ID " + str(rejoinID) + " and key " + rejoinKey)
	return render_template('index.html', rejoining=True, rejoinRoomID=rejoinID, rejoinRoomKey=rejoinKey, now=datetime.datetime.utcnow)

@app.route('/spectate')
@app.route('/spectate/<spectateID>')
@app.route('/spectate/<spectateID>/<spectateKey>')
def spectate(spectateID=None, spectateKey=None):
	"""Connect the user to a room as a spectator, then serve the web page"""
	print("Serve spectate")
	splitKey = spectateKey.split('-')
	spectateKey = splitKey[0] + ' ' + splitKey[1]
	if spectateID and spectateKey:
		print("Spectate with ID " + str(spectateID) + " and key " + spectateKey)
	return render_template('index.html', spectating=True, spectateID=spectateID, spectateKey=spectateKey, now=datetime.datetime.utcnow)


def application(environ, start_response):
	"""PEP 0333 WSGI entry point"""
	path = environ["PATH_INFO"]
	if path == "/data":
		if environ["wsgi.websocket"]:
			ws = environ["wsgi.websocket"]
			if ws:
				handle_websocket(ws)
	else:
		return app(environ, start_response)
	return

if __name__ == "__main__":
	host,port = "0.0.0.0",5000
	print("Serving on port " + str(port))
	server = WSGIServer((host, port), application, handler_class=WebSocketHandler)
	server.serve_forever()
