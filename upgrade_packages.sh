#!/bin/sh

pip freeze > requirements.txt
cp requirements.txt requirements.txt.old
pip_upgrade_outdated -3v
pip freeze > requirements.txt
