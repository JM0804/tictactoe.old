# Tic Tac Toe

A Tic Tac Toe web-app built in Python with Flask

## Features

- No account required - simply share the group name and password to play with friends
- Responsive design - play comfortably on any device

## Set-up

The following packages are required in order to run Tic Tac Toe:

- python3
- python3-dev
- python3-pip
- python3-venv

It is recommended that you set up the development environment by creating the virtual environment. Run the following command:

    $ python3 -m venv venv

And run this command to enter the virtual environment:

    $ source venv/bin/activate

You will need to run this at the start of every console session.

Once in the virtual environment, first make sure you have the latest version of pip3:

    $ pip3 install --upgrade pip

Then install the required python packages:

    $ pip3 install -r requirements.txt

You must then create the database by running the following commands:

    $ export FLASK_APP=wsgi.py
    $ flask db init
    $ flask db migrate
    $ flask db upgrade

## Requirements

- Python 3.6.5
- Flask 1.0.2
- Python package requirements are listed in `requirements.txt` and can be installed with the command: `pip3 install -r requirements.txt`

## Running the app

Please first ensure you have entered the virtual environment and installed all the necessary requirements

To run the built-in WSGI server (for testing), run the following command:

    $ python3 wsgi.py

To run the app with `gunicorn` (for production), run the following command:

    $ gunicorn -c gunicorn_config.py wsgi:application

Or simply run the shell script:

    $ ./run.sh

## Update the database

After making changes to `models.py`, the database needs to be updated in order for the changes to take effect. To do this, run the following commands:

    $ flask db migrate
    $ flask db upgrade
